" You want Vim, not vi. When Vim finds a vimrc, 'nocompatible' is set anyway.
" We set it explicitely to make our position clear!
set nocompatible

filetype plugin indent on  " Load plugins according to detected filetype.
syntax on                  " Enable syntax highlighting.

set autoindent             " Indent according to previous line.
set expandtab              " Use spaces instead of tabs.
set softtabstop =4         " Tab key indents by 2 spaces.
set shiftwidth  =4         " >> indents by 2 spaces.
set shiftround             " >> indents to next multiple of 'shiftwidth'.

set backspace   =indent,eol,start  " Make backspace work as you would expect.
set hidden                 " Switch between buffers without having to save first.
set laststatus  =2         " Always show statusline.
set display     =lastline  " Show as much as possible of the last line.

set showmode               " Show current mode in command-line.
set showcmd                " Show already typed keys when more are expected.

set incsearch              " Highlight while searching with / or ?.
set nohlsearch             " Don't keep matches highlighted.

set ttyfast                " Faster redrawing.
set lazyredraw             " Only redraw when necessary.

set number                 " Enable numbering
set relativenumber         " Enable relative numbering

set cursorline             " Find the current line quickly.
set wrapscan               " Searches wrap around end-of-file.
set report      =0         " Always report changed lines.
set synmaxcol   =200       " Only highlight the first 200 columns.

set scrolloff=5            " Keep 5 lines from above visible when scrolling down
set history=200            " Keep 200 Ex commands in history
set dictionary-=/usr/share/dict/words dictionary+=/usr/share/dict/words " Dictrionary autocomplete

" Run programs from vim itself
autocmd FileType python nnoremap <buffer> ' :exec '!python3' shellescape(@%, 1)<cr>
autocmd FileType python nnoremap <buffer> " :exec '!python' shellescape(@%, 1)<cr>

" Keybinds
" Toggle relative numbering
map <F12> :set relativenumber!<CR>
imap jk <ESC>
nnoremap <C-n> :bnext<CR>
nnoremap <C-p> :bprevious<CR>

" Plugin manager (vim-plug)
" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Plugins
" gruvbox theme
Plug 'morhetz/gruvbox'
" fugitive.vim
Plug 'tpope/vim-fugitive'
" file:line plugin
Plug 'bogado/file-line'
" CtrlP file search plugin
Plug 'ctrlpvim/ctrlp.vim'

" Initialize plugin system
call plug#end()

" Gruvbox themeing and color contrast
let g:gruvbox_italic=1
set background=dark
let g:gruvbox_contrast_dark='hard'
set termguicolors
colorscheme gruvbox

"if &term =~ "xterm"
"  " 256 colors
"  let &t_Co = 256
"  " restore screen after quitting
"  let &t_ti = "\<Esc>7\<Esc>[r\<Esc>[?47h"
"  let &t_te = "\<Esc>[?47l\<Esc>8"
"  if has("terminfo")
"    let &t_Sf = "\<Esc>[3%p1%dm"
"    let &t_Sb = "\<Esc>[4%p1%dm"
"  else
"    let &t_Sf = "\<Esc>[3%dm"
"    let &t_Sb = "\<Esc>[4%dm"
"  endif
"endif
